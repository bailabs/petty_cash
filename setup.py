from setuptools import setup, find_packages
version = '0.0.1'

setup(
	name='petty_cash',
	version=version,
	description='Petty Cash Module',
	author='Bai Web and Mobile Lab',
	author_email='ccfiel@bai.ph',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=("frappe",)
)
