// Copyright (c) 2016, CSharp Inc. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Petty Cash', {
	refresh: function(frm) {

	},
	validate: function(frm) {
        let total_amount = 0;
        let total_other_currency = 0;
        for(let line of frm.doc.lines){
            total_amount += line.amount;
            total_other_currency += line.other_currency;
        }
        try{
            frm.set_value('total_amount',total_amount);
            frm.set_value('total_other_currency',total_other_currency);
        }catch(e){
            console.log(e);
        }
	}
});


frappe.ui.form.on("Petty Cash", "onload", function (frm) {
    frm.fields_dict.lines.grid.get_field('branch').get_query =
        function () {
            return {
                filters: {
                    "company": frm.doc.company
                }
            }
        }

        frm.fields_dict.lines.grid.get_field('account').get_query =
        function () {
            return {
                filters: {
                    "company": frm.doc.company
                }
            }
        }

});
