# -*- coding: utf-8 -*-
# Copyright (c) 2015, CSharp Inc. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
from erpnext.controllers.accounts_controller import AccountsController
import frappe
from datetime import date
from frappe.utils import flt

def validate_accounting_period(gl_map):
    accounting_periods = frappe.db.sql(""" SELECT
            ap.name as name
        FROM
            `tabAccounting Period` ap, `tabClosed Document` cd
        WHERE
            ap.name = cd.parent
            AND ap.company = %(company)s
            AND cd.closed = 1
            AND cd.document_type = %(voucher_type)s
            AND %(date)s between ap.start_date and ap.end_date
            """, {
                'date': gl_map[0].posting_date,
                'company': gl_map[0].company,
                'voucher_type': gl_map[0].voucher_type
            }, as_dict=1)

    if accounting_periods:
        frappe.throw(_("You cannot create or cancel any accounting entries within in the closed Accounting Period {0}")
            .format(frappe.bold(accounting_periods[0].name)), ClosedAccountingPeriod)


def delete_gl_entries(gl_entries=None, voucher_type=None, voucher_no=None, adv_adj=False, update_outstanding="Yes"):
    from erpnext.accounts.doctype.gl_entry.gl_entry import update_outstanding_amt
    if not gl_entries:
        gl_entries = frappe.db.sql("""
            select account, posting_date, party_type, party, cost_center, fiscal_year,voucher_type,
            voucher_no, against_voucher_type, against_voucher, cost_center, company
            from `tabGL Entry`
            where voucher_type=%s and voucher_no=%s
            for update""", (voucher_type, voucher_no), as_dict=True)

    if gl_entries:
        validate_accounting_period(gl_entries)

    frappe.db.sql("""delete from `tabGL Entry` where voucher_type=%s and voucher_no=%s""",
        (voucher_type or gl_entries[0]["voucher_type"], voucher_no or gl_entries[0]["voucher_no"]))
    
    frappe.db.sql("""delete from `tabPayment Ledger Entry` where voucher_type=%s and voucher_no=%s""",
        (voucher_type or gl_entries[0]["voucher_type"], voucher_no or gl_entries[0]["voucher_no"]))

    for entry in gl_entries:
        if entry.get("against_voucher") and update_outstanding == 'Yes' and not adv_adj:
            update_outstanding_amt(entry["account"], entry.get("party_type"), entry.get("party"), entry.get("against_voucher_type"),
                entry.get("against_voucher"), on_cancel=True)


class PettyCash(AccountsController):
    def is_employee(self):
        user = frappe.get_user()
        for data in user.roles:
            has_role = frappe.get_doc("Has Role", data.name)
            role = frappe.get_doc("Role", has_role.role)
            if str(role.role_name).lower() == 'employee':
                return True
            
        
    def validate(self):        
        if self.is_employee():
            user = frappe.get_user()
            employee = frappe.db.sql("""SELECT name FROM tabEmployee where prefered_email=%s""", user.email)
            self.employee = employee[0][0]
            if len(employee)<=0:
                frappe.throw(user.email + ' not found in Employee List. Please contact your system administrator.')
        
        if not self.employee:
            frappe.throw('Employee field is required. Please contact your system administrator.')

        is_manager = False
        is_user = False
        for role in frappe.get_roles(frappe.get_user().name):
            if role == 'Accounts User':
                is_manager = True
            if role == 'Accounts Manager':
                is_user = True

        if not is_manager and is_user:
            for data in self.lines:
                data.account = None

        if not self.posting_date:
            self.posting_date = date.today()

        company = frappe.get_doc("Company", self.company)
        self.company = company.name
        if self.fund_type == 'Revolving':
            self.beg_balance = account_balance(frappe.db.get_value("Petty Cash Settings", None, "revolving_fund_account"), self.company, self.employee)
        else:
            self.beg_balance = account_balance(frappe.db.get_value("Petty Cash Settings", None, "petty_cash_account"), self.company, self.employee)

        amount = 0
        for data in self.lines:
            if data.amount:
                amount += data.amount
        if not self.beg_balance:
            self.beg_balance = 0
        self.ending_balance = self.beg_balance - amount

        doc_employee = frappe.get_doc("Employee", self.employee)
        self.title = str(self.posting_date) + ' ' + doc_employee.employee_name
        self.balances = str(self.beg_balance) + '/' + str(self.ending_balance)

    def on_submit(self):
        print('on_submit')
        print(self.fund_type)
        if str(self.fund_type).strip() == '' or self.fund_type == None:
            frappe.throw('Fund Type is required. Please contact your system administrator.')
        self.create_gl_entry()

    def create_gl_entry(self):
        # user = frappe.get_doc("User", self.owner)
        # employee = frappe.db.sql("""SELECT name FROM tabEmployee where prefered_email=%s""", user.email)
        company = frappe.get_doc("Company", self.company)
        from erpnext.accounts.general_ledger import make_gl_entries
        gl_map = []
        if self.remarks:
            remarks = self.remarks
        else:
            remarks = "Payment"

        amount = 0
        for data in self.lines:
            if not data.account:
                frappe.throw("{0} has no account assigned".format(data.description))

            amount += data.amount
            if data.employee:
                # employee_doc = frappe.get_doc('Employee', self.employee)
                gl_map.append(
                    self.get_gl_dict({
                        "account": data.account,
                        "credit": flt(0),
                        "debit": flt(data.get("amount")),
                        "party_type": "Employee",
                        "party": data.employee,
                        "remarks": data.description,
                        "cost_center": data.cost_center,
                    }))
            else:
                gl_map.append(
                    self.get_gl_dict({
                        "account": data.account,
                        "credit": flt(0),
                        "debit": flt(data.get("amount")),
                        "remarks": data.description,
                        "cost_center": data.cost_center,
                    }))

        if self.fund_type == 'Revolving':
            account = frappe.db.get_value("Petty Cash Settings", None, "revolving_fund_account")
        else:
            account = frappe.db.get_value("Petty Cash Settings", None, "petty_cash_account")

        gl_map.append(
            self.get_gl_dict({
                "account": account,
                "party_type": "Employee",
                "party": self.employee,
                "credit": flt(amount),
                "remarks": remarks,
                "cost_center": company.cost_center
            }))
        if gl_map:
            make_gl_entries(gl_map, cancel=0, adv_adj=0)

    def on_cancel(self):
        delete_gl_entries(voucher_type=self.doctype, voucher_no=self.name)


def account_balance(account, company, party):
    balance = frappe.db.sql("""SELECT sum(debit) - sum(credit) FROM `tabGL Entry`
                            WHERE docstatus = 1 AND Account = %s
                            AND company = %s AND party_type='Employee' AND party = %s""", (account, company, party))
    return balance[0][0]
